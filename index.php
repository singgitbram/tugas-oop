<?php

require './animal.php';
require './Ape.php';
require './Frog.php';

$sheep = new Animal("shaun the sheep");
echo "<br>";
echo $sheep->get_name(); // "shaun"
echo "<br>";
echo $sheep->get_legs(); // 2
echo "<br>";
echo $sheep->get_cold_blooded(); // false
echo "<br>";
echo "<br>";
$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"
echo "<br>";
echo $sungokong->get_name();
echo "<br>";
echo $sungokong->get_legs();
echo "<br>";
echo $sungokong->get_cold_blooded();
echo "<br>";
echo "<br>";
$kodok = new Frog("buduk");
$kodok->jump(); // "hop hop"
echo "<br>";
echo $kodok->get_name();
echo "<br>";
echo $kodok->get_legs();
echo "<br>";
echo $kodok->get_cold_blooded();

$murid = [22,33,44];
$murid[] = 100;
print_r($murid);