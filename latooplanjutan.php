<?php

trait Hewan
{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi()
    {
        echo $this->nama . " sedang " . $this->keahlian . "<br><br>";
    }
};

trait Fight
{
    public $attackPower;
    public $defencePower;

    public function serang($lawan)
    {
        echo $this->nama . " sedang menyerang " . $lawan->nama . "<br><br>";
    }

    public function diserang($lawan)
    {
        echo $this->nama . " sedang diserang" . "<br><br>";
        $this->darah = $this->darah - $lawan->attackPower / $this->defencePower;
    }
};

class Elang
{
    use Hewan, Fight;
    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getInfoHewan()
    {
        echo "jenis saya " . get_class($this) . "<br>";
        echo "nama saya " . $this->nama . "<br>";
        echo "jumlah kaki saya " . $this->jumlahKaki . "<br>";
        echo "keahlian saya " . $this->keahlian . "<br>";
        echo "darah saya " . $this->darah . "<br>";
        echo "attack saya " . $this->attackPower . "<br>";
        echo "defense saya " . $this->defencePower . "<br><br>";
    }
};

class Harimau
{
    use Hewan, Fight;
    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getInfoHewan()
    {
        echo "jenis saya " . get_class($this) . "<br>";
        echo "nama saya " . $this->nama . "<br>";
        echo "jumlah kaki saya " . $this->jumlahKaki . "<br>";
        echo "keahlian saya " . $this->keahlian . "<br>";
        echo "darah saya " . $this->darah . "<br>";
        echo "attack saya " . $this->attackPower . "<br>";
        echo "defense saya " . $this->defencePower . "<br><br>";
    }
};

$elang = new Elang("elang1");
$harimau = new Harimau("harimau1");

$elang->atraksi();
$harimau->atraksi();

$elang->getInfoHewan();
$harimau->getInfoHewan();

$elang->serang($harimau);
$elang->diserang($harimau);

$harimau->serang($elang);
$harimau->diserang($elang);

$elang->getInfoHewan();
$harimau->getInfoHewan();
